const express = require("express");
const dotenv = require("dotenv").config({ path: "./config.env" });
const app = express();
const mongoose = require("mongoose");
const Data = require("./models/dataModel");
const { User, validate } = require("./models/userModel");
const methodOverride = require("method-override");

const auth = require("./auth");
const bcrypt = require("bcrypt");

const DB = process.env.DATABASE;
mongoose
  .connect(DB, { useNewUrlParser: true })
  .then(() => {
    console.log("Connection Successfully");
  })
  .catch(err => {
    console.log("ERROR", err.message);
  });

// use view engine pug
app.set("view engine", "pug");

// serve static files
app.use(express.static(__dirname + "/public"));

// Using Request Body Parser

app.use(express.json());
app.use(express.urlencoded({ extended: false }));

// Using Method Override
app.use(methodOverride("_method"));

//ROUTES

// Search Route

app.get("/search/:page?/:query?", async (req, res, next) => {
  const searchQuery = req.query.search ? req.query.search : req.params.query;
  // Page Url
  const orgUrl = req.originalUrl;
  const splitedUrl = orgUrl.split("?");

  try {
    // Pagination
    const perPage = 11;
    const page = req.params.page ? parseInt(req.params.page) : 1;

    // Find Searched Results
    const pageData = await Data.find({
      $or: [
        { firstName: searchQuery },
        { lastName: searchQuery },
        { email: searchQuery }
      ]
    })
      .skip(perPage * page - perPage)
      .limit(perPage)
      .exec();

    // The Number of Searched Results

    const count = await Data.find({
      $or: [
        { firstName: searchQuery },
        { lastName: searchQuery },
        { email: searchQuery }
      ]
    }).exec();
    res.render("search", {
      searchQuery,
      pageUrl: splitedUrl[0],
      pageData,
      current: page,
      pages: Math.ceil(count.length / perPage),
      count
    });
  } catch (err) {
    next(err);
  }
});

// Authentification Routes

app.get("/login", (req, res) => {
  res.render("auth/login");
});

app.post("/login", async (req, res, next) => {});

app.get("/register", (req, res) => {
  res.render("auth/register");
});

app.post("/register", async (req, res) => {
  // valdiate the requst body first
  const user = req.body;
  const { error } = validate(user);
  console.log(error);
  if (error) {
    return res.render("/register", { error: error }, function(err, html) {
      if (err) console.error(err);
    });
  }

  try {
    const createUser = await User.create(user);
    res.redirect("/login");
  } catch (err) {
    res.render("auth/register");
  }
});

// Crud Routes

app.get("/:page?", async (req, res, next) => {
  try {
    // Pagination
    const perPage = 10;
    const page = req.params.page ? parseInt(req.params.page) : 1;

    // Find Page Results

    const pageData = await Data.find()
      .skip(perPage * page - perPage)
      .limit(perPage);

    // Count Documents
    const count = await Data.countDocuments();
    res.render("index", {
      pageData: pageData,
      current: page,
      pages: Math.ceil(count / perPage)
    });
  } catch (err) {
    next(err);
  }
});

// Edit Data

app.get("/data/:id/edit", async (req, res, next) => {
  try {
    const data = await Data.findById(req.params.id);
    res.render("edit", { data: data });
  } catch (err) {
    next(err);
  }
});

// Update Data

app.put("/data/:id", async (req, res, next) => {
  try {
    const data = {
      firstName: req.body.firstName,
      lastName: req.body.lastName,
      email: req.body.email
    };
    const id = req.params.id;
    await Data.findByIdAndUpdate(id, data);
    res.redirect("back");
  } catch (err) {
    next(err);
  }
});

// Delete Data

app.delete("/data/:id", async (req, res, next) => {
  try {
    await Data.findByIdAndDelete(req.params.id);
    res.redirect("back");
  } catch (err) {
    next(err);
  }
});

// Insert Data

app.post("/data", (req, res) => {
  const data = new Data({
    firstName: req.body.firstName,
    lastName: req.body.lastName,
    email: req.body.email
  });
  data
    .save()
    .then(doc => {
      console.log(doc);
    })
    .catch(err => {
      console.log("ERROR", err);
    });
  res.redirect("/");
});

// Server Running

app.listen(8000, process.env.IP);
