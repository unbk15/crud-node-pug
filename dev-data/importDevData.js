const fs = require("fs");
const faker = require("faker");
const mongoose = require("mongoose");
const Data = require("./../models/data");

// 1) Generate the data

let dataArray = [];

for (let index = 0; index < 1000; index++) {
  let entry = {
    firstName: `${faker.name.firstName()}`,
    lastName: `${faker.name.lastName()}`,
    email: `${faker.internet.email()}`
  };
  dataArray.push(entry);
}

JSON.stringify(dataArray);

// 2) Write the file

// fs.writeFile(`${__dirname}/data.json`, JSON.stringify(dataArray), err => {
//   if (err) console.log("Error");
//   else console.log("Write Successfuly");
// });

// 3) Connect to the DB
const DB = `mongodb+srv://regiso:RegisoCrudApp2019@cluster0-g00ca.mongodb.net/test?retryWrites=true&w=majority`;

mongoose
  .connect(DB, { useNewUrlParser: true })
  .then(() => {
    console.log("Connection Successfully");
  })
  .catch("Something went wrong with the DB connection");

// 4) Delete Data In The Database

const deleteData = async () => {
  try {
    await Data.deleteMany();
    console.log("Deleted Succesfully");
  } catch (err) {
    console.log(err);
  }
  process.exit();
};

// 5) Insert Data In The Database
const importData = async () => {
  try {
    await Data.create(dataArray);
    console.log("Data Succesfully loaded");
  } catch (err) {
    console.log(err);
  }
  process.exit();
};

// 6) Choose the appropriate method

if (process.argv[2] === "--import") {
  importData();
} else if (process.argv[2] === "--delete") {
  deleteData();
}

// node importDevData.js --import
// node importDevData.js --delete
