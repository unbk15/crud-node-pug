const mongoose = require("mongoose");
const validator = require("validator");

const dataSchema = new mongoose.Schema({
  firstName: {
    type: String
  },
  lastName: {
    type: String
  },
  email: {
    type: String,
    validate: {
      validator: validator.isEmail,
      message: "{VALUE} is not a valid email",
      isAsync: false
    }
  }
});

module.exports = mongoose.model("Data", dataSchema);
