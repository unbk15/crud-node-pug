$(document).ready(function() {
  // Passing data from the trigger modal delete button to the delete modal

  $(".modalDeleteButton").on("click", function(event) {
    // Get the id
    let id = event.currentTarget.dataset.id;

    // route to delete the entry
    let deleteRoute = `data/${id}?_method=DELETE`;
    $("#deleteModalForm").attr("action", deleteRoute);
  });

  // Passing data from the update trigger modal button to the update modal

  $(".app-update-data").on("click", function(event) {
    // 1) Get the values from the hidden inputs
    let firstName = event.currentTarget.childNodes[0].value;
    let lastName = event.currentTarget.childNodes[1].value;
    let email = event.currentTarget.childNodes[2].value;
    let _id = event.target.attributes[2].value;

    // 2) Send the values into the editModal
    $("#app-update-firstName").val(firstName);
    $("#app-update-lastName").val(lastName);
    $("#app-update-email").val(email);

    // 3) Update the action form

    let updateFormAction = `/data/${_id}?_method=PUT`;
    $("#updateModalForm").attr("action", updateFormAction);
  });

  // Display the search form
  $("#app-search-icon").on("click", function() {
    // Change search icon
    if ($(this).text() === "search") {
      $(this).text("close");
    } else {
      $(this).text("search");
    }
    $("#app-search-form").toggleClass("d-block");
  });
});
